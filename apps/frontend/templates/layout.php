<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php include_http_metas() ?>
<?php include_metas() ?>
<?php include_title() ?>
<link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<div id="art-page-background-simple-gradient"></div>
<div id="art-main">
<div class="art-Sheet">
<div class="art-Sheet-tl"></div>
<div class="art-Sheet-tr"></div>
<div class="art-Sheet-bl"></div>
<div class="art-Sheet-br"></div>
<div class="art-Sheet-tc"></div>
<div class="art-Sheet-bc"></div>
<div class="art-Sheet-cl"></div>
<div class="art-Sheet-cr"></div>
<div class="art-Sheet-cc"></div>
<div class="art-Sheet-body">
<div class="art-Header">
<div class="art-Header-jpeg"></div>
<div class="art-Logo">
<h1 id="name-text" class="art-Logo-name">
<?php echo link_to('Sir Tech', 'main/index') ?>
</h1>
<div id="slogan-text" class="art-Logo-text">Reparación y mantenimiento
de computadores a domicilio</div>
</div>


</div>
<div class="art-contentLayout">
<div class="art-sidebar1">
<div class="art-Block">
<div class="art-Block-body">

<div class="art-BlockHeader">
<div class="art-header-tag-icon">
<div class="t">Menú principal</div>
</div>
</div>
<div class="art-BlockContent">
<div class="art-BlockContent-tl"></div>
<div class="art-BlockContent-tr"></div>
<div class="art-BlockContent-bl"></div>
<div class="art-BlockContent-br"></div>
<div class="art-BlockContent-tc"></div>
<div class="art-BlockContent-bc"></div>
<div class="art-BlockContent-cl"></div>
<div class="art-BlockContent-cr"></div>
<div class="art-BlockContent-cc"></div>
<div class="art-BlockContent-body">

<ul class="menu">
	<li id="current" class="active item1"><?php echo link_to('Home', 'main/index') ?>
	</li>
	<li id="current" class="active item1"><?php echo link_to('Solicitar soporte técnico', 'main/request') ?>
	</li>
	<li id="current" class="active item1"><?php echo link_to('Estado de solicitud', 'main/consultateState') ?>
	</li>
</ul>
<div class="cleared"></div>
</div>
</div>


<div class="cleared"></div>
</div>
</div>

</div>
<div class="art-content-sidebar2">
<div class="art-Post">
<div class="art-Post-tl"></div>
<div class="art-Post-tr"></div>
<div class="art-Post-bl"></div>
<div class="art-Post-br"></div>
<div class="art-Post-tc"></div>
<div class="art-Post-bc"></div>
<div class="art-Post-cl"></div>
<div class="art-Post-cr"></div>
<div class="art-Post-cc"></div>
<div class="art-Post-body">
<div class="art-Post-inner"><?php echo $sf_content ?>
<br></br>
</div>
</div>
<div class="cleared"></div>
</div>
</div>
</div>
<div class="cleared"></div>


<div class="art-Footer">
<div class="art-Footer-inner">
<div class="art-Footer-text">
<p>Copyright © 2009 Sir Tech</p>
</div>
</div>
<div class="art-Footer-background"></div>
</div>

<div class="cleared"></div>
</div>
</div>
<div class="cleared"></div>
</div>
</body>
</html>
