<?php

/**
 * principal actions.
 *
 * @package    sirtech
 * @subpackage main
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class mainActions extends sfActions
{
  protected static $captcha_length = 8;
  /**
   * Executes index action
   *
   * @param sfRequest $request A request object
   */
  public function executeIndex(sfWebRequest $request)
  {

  }
  public function executeRequest(sfWebRequest $request)
  {
    $captcha = $this->getUser()->getAttribute('captcha','');
    $this->form = new RequestForm();
    if($request->isMethod('post') && $captcha!='') {
      $captchaValidator = new sfValidatorCaptcha(array('captcha' => $captcha, 'length' => mainActions::$captcha_length));
      $this->form->setValidator('captcha', $captchaValidator);
      $this->form->bind($request->getParameter('request'));
      if($this->form->isValid()) {
        $this->redirect('main/thankUser');
      }
    }
  }
  public function executeGenerateCaptcha() {
    $response = $this->getResponse();
    $response->setContentType("image/gif");
    $response->send();

    $alphabet = "1234567890abcdefghijklmnopqrstuvwxyz";
    $key = "";
    for($i=0;$i<mainActions::$captcha_length;$i++) {
      $key .= $alphabet{rand(0,strlen($alphabet)-1)};
    }

    $this->getUser()->setAttribute('captcha',$key);

    $captcha = imagecreatefromgif("images/bgcaptcha.gif");
    $colText = imagecolorallocate($captcha, 0, 0, 0);
    imagestring($captcha, 5, 16, 7, $key, $colText);
    imagegif($captcha);
    return sfView::NONE;
  }
  public function executeConsultateState() {
    $this->form = new ConsultateForm();
  }
  public function executeThankUser() {
    
  }
}