<h2 class="art-PostHeader"><img src="/images/PostHeaderIcon.png"
	alt="PostHeaderIcon" height="11" width="11"> <span
	class="componentheading">Solicitar soporte técnico</span></h2>

<br></br>

<?php use_helper('Form')?>

<?php echo form_tag('main/request',array('name' => 'request_form'))?>
<table>
	<tr>
		<td><?php echo $form['requester_name']->renderLabel()?>:</td>
		<td><?php echo $form['requester_name']?> <?php if($form['requester_name']->hasError()):?>
		<img src="/images/error.png" class="error_icon"></img><span
			class="error_message"> <?php echo $form['requester_name']->getError()?></span>
			<?php endif;?></td>
	</tr>
	<tr>
		<td><?php echo $form['phone_number']->renderLabel()?>:</td>
		<td><?php echo $form['phone_number']?> <?php if($form['phone_number']->hasError()):?>
		<img src="/images/error.png" class="error_icon"></img><span
			class="error_message"> <?php echo $form['phone_number']->getError()?></span>
			<?php endif;?></td>
	</tr>
	<tr>
		<td><?php echo $form['department']->renderLabel()?>:</td>
		<td><?php echo $form['department']?> <?php if($form['department']->hasError()):?>
		<img src="/images/error.png" class="error_icon"></img><span
			class="error_message"> <?php echo $form['department']->getError()?></span>
			<?php endif;?></td>
	</tr>
	<tr>
		<td><?php echo $form['city']->renderLabel()?>:</td>
		<td><?php echo $form['city']?> <?php if($form['city']->hasError()):?>
		<img src="/images/error.png" class="error_icon"></img><span
			class="error_message"> <?php echo $form['city']->getError()?></span>
			<?php endif;?></td>
	</tr>
	<tr>
		<td><?php echo $form['address']->renderLabel()?>:</td>
		<td><?php echo $form['address']?> <?php if($form['address']->hasError()):?>
		<img src="/images/error.png" class="error_icon"></img><span
			class="error_message"> <?php echo $form['address']->getError()?></span>
			<?php endif;?></td>
	</tr>
	<tr>
		<td><?php echo $form['problem_description']->renderLabel()?>:</td>
		<td><?php echo $form['problem_description']?> <?php if($form['problem_description']->hasError()):?>
		<img src="/images/error.png" class="error_icon"></img><span
			class="error_message"> <?php echo $form['problem_description']->getError()?></span>
			<?php endif;?></td>
	</tr>
	<tr>
		<td><label>Código de verificación:</label></td>
		<td><img src="<?php echo url_for('main/generateCaptcha')?>"></img></td>
	</tr>
	<tr>
		<td><?php echo $form['captcha']->renderLabel()?>:</td>
		<td><?php echo $form['captcha']?> <?php if($form['captcha']->hasError()):?>
		<img src="/images/error.png" class="error_icon"></img><span
			class="error_message"> <?php echo $form['captcha']->getError()?></span>
			<?php endif;?></td>
	</tr>
	<tr>
		<td><br></br>
		</td>
	</tr>
	<tr>
		<td><img src="/images/boton_enviar.png"
			onclick="document.request_form.submit()"></img></td>
	</tr>
</table>
</form>
