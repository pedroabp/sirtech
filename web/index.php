<?php

//	  TODO Validar el navegador utilizado por el usuario(User-Agent) 
//    
//	  $this->userAgent = $request->getHttpHeader('User-Agent');

require_once(dirname(__FILE__).'/../config/ProjectConfiguration.class.php');

$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', false);
sfContext::createInstance($configuration)->dispatch();
