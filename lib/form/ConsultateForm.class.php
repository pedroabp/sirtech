<?php
class ConsultateForm extends sfForm
{
	public function configure()
	{
		$this->setWidgets(array(
      'request_id' => new sfWidgetFormInput()
		));

		$this->widgetSchema->setLabel('request_id', 'Código de la solicitud');

		$this['request_id']->getWidget()->setAttribute('class','id_field');
	}
}
?>