<?php
class RequestForm extends sfForm
{
  public function configure()
  {
    // TODO Los departamentos deben ser leídos de la base de datos
    $departments = array('Valle del Cauca');
    $departmentFormSelect = new sfWidgetFormSelect(array('choices' => $departments), array('class' => 'form_field'));

    // TODO Las ciudades deben ser leídas de la base de datos
    $cities = array('Candelaria');
    $cityFormSelect = new sfWidgetFormSelect(array('choices' => $cities), array('class' => 'form_field'));

    $this->setWidgets(array(
      'requester_name'    => new sfWidgetFormInput(array(), array('class' => 'form_field')),
      'phone_number'   => new sfWidgetFormInput(array(), array('class' => 'form_field')),
		'department' => $departmentFormSelect,
		'city' => $cityFormSelect,
      'address'   => new sfWidgetFormInput(array(), array('class' => 'address_field')),
      'problem_description' => new sfWidgetFormTextarea(array(), array('class' => 'form_field')),
		'captcha' => new sfWidgetFormInputCaptcha(array(), array('class' => 'form_field'))
    ));
    
    $this->widgetSchema->setLabel('requester_name', 'Nombre del solicitante');
    $this->widgetSchema->setLabel('phone_number', 'Teléfono fijo');
    $this->widgetSchema->setLabel('department', 'Departamento');
    $this->widgetSchema->setLabel('city', 'Ciudad');
    $this->widgetSchema->setLabel('address', 'Dirección');
    $this->widgetSchema->setLabel('problem_description', 'Descripción del problema');
    $this->widgetSchema->setLabel('captcha', 'Ingrese el código de verificación');

    $requesterNameValidator = new sfValidatorString(array('min_length' => 5, 'max_length' => 25));
    $requesterNameValidator->addMessage('required', 'Debe escribir su nombre');
    $requesterNameValidator->addMessage('min_length', 'Mínimo %min_length% caracteres');
    $requesterNameValidator->addMessage('max_length', 'Máximo %max_length% caracteres');
    $this->setValidator('requester_name', $requesterNameValidator);
    
    $phoneNumberValidator = new sfValidatorInteger(array('required' => false), array('invalid' => 'No es un número válido'));
    $this->setValidator('phone_number', $phoneNumberValidator);
    
    $departmentValidator = new sfValidatorChoice(array('choices' => array_keys($departments)));
    $this->setValidator('department', $departmentValidator);
    
    $cityValidator = new sfValidatorChoice(array('choices' => array_keys($cities)));
    $this->setValidator('city', $cityValidator);
        
    $addressValidator = new sfValidatorString(array('min_length' => 10, 'max_length' => 32));
    $addressValidator->addMessage('required', 'Debe escribir su dirección');
    $addressValidator->addMessage('min_length', 'Mínimo %min_length% caracteres');
    $addressValidator->addMessage('max_length', 'Máximo %max_length% caracteres');
    $this->setValidator('address', $addressValidator);
    
    $problemDescriptionValidator = new sfValidatorString(array('min_length' => 25, 'max_length' => 150));
    $problemDescriptionValidator->addMessage('required', 'Debe escribir una breve descripción del problema');
    $problemDescriptionValidator->addMessage('min_length', 'Mínimo %min_length% caracteres');
    $problemDescriptionValidator->addMessage('max_length', 'Máximo %max_length% caracteres');
    $this->setValidator('problem_description', $problemDescriptionValidator);
    
    $this->widgetSchema->setNameFormat('request[%s]');
  }
}
?>